# README #
This inventory management system is being designed for an individual for the purposes of tracking inventory and generating usage reports.
The goal is to replace an over-burdened Excel file and pivot tables with a simple user interface and SQLite databases.

Completed tasks:
Pages for entering, saving, and editing item and project information.
Page for tracking inventory, including adding to and removing from inventory
Editing of past inventory transactions
Visualization of inventory transactions
Added filters for viewing transactions
Preferences page for things like tax rates and backup location

Current tasks:
-Add reporting as requested by client
-Add preferences as needed

Future goals:
-Add special functionality for government projects and items
-Ability to back up inventory
-Add descriptions and tool tips
-Export to CSV
-Import from CSV
-Clean up