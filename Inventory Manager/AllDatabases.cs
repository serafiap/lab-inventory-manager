﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLiteHelpers;

namespace InventoryManager
{
    static class MainDatabase
    {
        private static table itemTable = new table(dbStrings.t_Items,
            new column(dbStrings.c_ItemID, Database.integer, true),
            new column(dbStrings.c_ItemNumber, Database.varchar, false, true),
            new column(dbStrings.c_ItemDescription, Database.varchar,false, false),
            new column(dbStrings.c_ItemUnits, Database.varchar, false, false),
            new column(dbStrings.c_ItemLocation, Database.varchar, false, false),
            new column(dbStrings.c_ItemCurrentQuantity, Database.real, "0"),
            new column(dbStrings.c_ItemCurrentPrice, Database.real, "0")
            );

        private static table projectsTable = new table(dbStrings.t_Projects,
            new column(dbStrings.c_ProjectID, Database.integer, true),
            new column(dbStrings.c_ProjectNumber, Database.varchar, false, true),
            new column(dbStrings.c_ProjectOrg, Database.varchar, false, false),
            new column(dbStrings.c_ProjectOC, Database.varchar, false, false),
            new column(dbStrings.c_ProjectDescription, Database.varchar, false, false),
            new column(dbStrings.c_ProjectType, Database.varchar, false, false),
            new column(dbStrings.c_ProjectIsGovernmental, Database.varchar, false, false)
            );

        private static table inventoryLog = new table(dbStrings.t_InventoryLog,
            new column(dbStrings.c_InventoryLogID, Database.integer, true),
            new column(dbStrings.c_InventoryDate, Database.varchar, false, false),
            new column(dbStrings.c_InventoryDayOrder, Database.integer, false, false),
            new column(dbStrings.c_InventoryAddRemove, Database.varchar, false, false),
            new column(dbStrings.c_ProjectNumber, Database.varchar, false, false),
            new column(dbStrings.c_ItemNumber, Database.varchar, false, false),
            new column(dbStrings.c_InventoryInitialQuantity, Database.real, false, false),
            new column(dbStrings.c_InventoryQuantityChange, Database.real, false, false),
            new column(dbStrings.c_inventoryFinalQuantity, Database.real, false, false),
            new column(dbStrings.c_InventoryInitialPrice, Database.real, false, false),
            new column(dbStrings.c_InventoryCheckInPrice, Database.real, false, false),
            new column(dbStrings.c_InventoryFinalPrice, Database.real, false, false),
            new column(dbStrings.c_InventorySurchargeRate, Database.real, false, false),
            new column(dbStrings.c_InventoryTaxRate, Database.real, false, false)
            );

        public static Database database = new Database(dbStrings.db_MainDatabaseName, itemTable, projectsTable, inventoryLog);

        //public static readonly Dictionary<string, int> tableDict = new Dictionary<string, int>()
        //{
        //    { dbStrings.t_ItemTemplates, 0 },
        //    { dbStrings.t_InventoryLog, 2 },
        //    { dbStrings.t_Projects, 1 }
        //};


    }
}
