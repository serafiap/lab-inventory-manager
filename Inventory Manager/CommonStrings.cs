﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManager
{
    public static class dbStrings
    {
        public const string db_MainDatabaseName = "MainDatabase";

        public const string t_InventoryLog = "InventoryLog";
        public const string t_Projects = "Projects";
        public const string t_Items = "Items";

        //Items Table Columns
        public const string c_ItemID = "ItemID";
        public const string c_ItemNumber = "ItemNumber";
        public const string c_ItemDescription = "ItemDescription";
        public const string c_ItemUnits = "Unit";
        public const string c_ItemLocation = "ItemLocation";
        public const string c_ItemCurrentQuantity = "CurrentQuantity";
        public const string c_ItemCurrentPrice = "CurrentPrice";

        //Projects Table Columns
        public const string c_ProjectID = "ProjectID";
        public const string c_ProjectNumber = "ProjectNumber";
        public const string c_ProjectOrg = "Org";
        public const string c_ProjectOC = "OC";
        public const string c_ProjectDescription = "ProjDesc";
        public const string c_ProjectType = "ProjType";
        public const string c_ProjectIsGovernmental = "IsGovt";

        //Inventory Log Table Columns
        public const string c_InventoryLogID = "ID";
        //public const string c_ItemNumber = "ItemNumber";
        //public const string c_ProjectNumber = "ProjectNumber";
        public const string c_InventoryDate = "Date";
        public const string c_InventoryDayOrder = "DayOrder";
        public const string c_InventoryAddRemove = "AddRemove";
        public const string c_InventoryInitialQuantity = "InitialQuantity";
        public const string c_InventoryQuantityChange = "QuantityChanged";
        public const string c_inventoryFinalQuantity = "FinalQuantity";
        public const string c_InventoryInitialPrice = "InitPrice";
        public const string c_InventoryCheckInPrice = "CheckInPrice";
        public const string c_InventoryFinalPrice = "FinalPrice";
        public const string c_InventorySurchargeRate = "SurchargeRate";
        public const string c_InventoryTaxRate = "TaxRate";

        public const string True = "True";
        public const string False = "False";

        public const string add = "Add"; //Used when "checking in" to inventory log
        public const string remove = "Remove"; //Used when "checking out" of inventory log
    }

    /// <summary>
    /// Class containing common strings oriented towards presenting information to the user
    /// </summary>
    public static class userStrings
    {
        public const string itemNumber = "Item Number";
        public const string itemDescription = "Item Description";
        public const string itemLocation = "Location";
        public const string itemQuantity = "Quantity";
        public const string unit = "Unit";
        public const string price = "Price Per Unit";
        public const string taxRate = "Tax Rate";
        public const string surcharge = "Surcharge";
        public const string date = "Transaction Date";
        public const string finalPrice = "Total Cost";
        public const string checkIn = "Check in";
        public const string checkOut = "Check out";
        public const string editEntry = "Edit existing entry";
        public const string filterAny = "Any";
        public const string yes = "Yes";
        public const string no = "No";
        public const string lowest = "Lowest";
        public const string highest = "Highest";
        public const string all = "All";
        public const string ten = "10";
        public const string twentyFive = "25";
        public const string fifty = "50";
        public const string seventyFive = "75";
        public const string oneHundo = "100";

    }

    public static class DirectoryStrings
    {
        public static string backupFolder = System.IO.Directory.GetCurrentDirectory() + "\\Backups";
        public static string reportsFolder = System.IO.Directory.GetCurrentDirectory() + "\\Reports";
        public static string projectChargeReportsFolder = reportsFolder + "\\Charge Reports";
        public static string inventoryReportsFolder = reportsFolder + "\\Inventory Reports";
    }

    public static class commonCollections
    {
        public static List<string> resultsToDisplayList = new List<string>()
        {
            userStrings.ten,userStrings.twentyFive,userStrings.fifty,userStrings.seventyFive,userStrings.oneHundo,userStrings.all
        };

        public static Dictionary<string, int> resultsToDisplayDict = new Dictionary<string, int>();
    }
}
