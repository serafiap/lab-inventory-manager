﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SQLiteHelpers;
using System.IO;

namespace InventoryManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NavigationWindow
    {
        public MainWindow()
        {
            Directory.CreateDirectory(DirectoryStrings.backupFolder);
            Directory.CreateDirectory(DirectoryStrings.projectChargeReportsFolder);
            Directory.CreateDirectory(DirectoryStrings.inventoryReportsFolder);
            if (Properties.Settings.Default.FirstRun)
            {
                PreferencesPage.useDefaultSettings();
                Properties.Settings.Default.FirstRun = false;
                Properties.Settings.Default.Save();
            }
            createDatabase();
            InitializeComponent();
        }

        private static void createDatabase()
        {
            Database db = MainDatabase.database;
            
            if (!File.Exists(db.dbFile))
            {
                db.createDatabase();
            }
        }
    }
}
