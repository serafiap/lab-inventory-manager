﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using System.Data;

namespace SQLiteHelpers
{
    class Database
    {
        public string dbName { get; }
        public string dbFile { get; }
        public SQLiteConnection connection { get; }
        public List<table> tableList { get; }

        public static readonly string varchar = "VARCHAR(255)";
        public static readonly string integer = "INTEGER";
        public static readonly string real = "REAL";
        public static readonly string ascending = "ASC";
        public static readonly string descending = "DESC";

        public Database(string dataBaseName, params table[] tables)
        {
            dbName = dataBaseName; 
            dbFile = dataBaseName + ".sqlite";
            connection = new SQLiteConnection(String.Format("Data Source={0};Version=3", dbFile));
            tableList = new List<table>();
            foreach (table tbl in tables)
            {
                tableList.Add(tbl);
            }
        }
        
        public void createDatabase()
        {
            if (!File.Exists(dbFile))
            {
                SQLiteConnection.CreateFile(dbFile);
            }

            //Create a table
            connection.Open();
            foreach (table tbl in tableList)
            {
                createTable(tbl, this.connection);
            }
            connection.Close();

        }

        private void createTable(table tbl, SQLiteConnection conn)
        {
            if (!tbl.Exists(conn))
            {
                new SQLiteCommand(tbl.GetCreationString(), conn).ExecuteNonQuery();
            }
        }

        public DataTable queryDatabase(SQLiteCommand retrievalCommand)
        {
            DataTable resultsTable = new DataTable();
            SQLiteConnection dbConn = this.connection;
            retrievalCommand.Connection = dbConn;
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(retrievalCommand);

            if (dbConn.State == ConnectionState.Closed)
            {
                dbConn.Open();
            }
            adapter.Fill(resultsTable);
            dbConn.Close();
            return resultsTable;
        }

        public void nonQueryDatabase(SQLiteCommand writeCommand)
        {
            SQLiteConnection dbConn = this.connection;
            if (dbConn.State == ConnectionState.Closed)
            {
                dbConn.Open();
            }
            writeCommand.Connection = dbConn;
            writeCommand.ExecuteNonQuery();
            dbConn.Close();
        }

        public bool checkExistence(SQLiteCommand scalarCommand)
        {
            bool exists;
            scalarCommand.Connection = this.connection;
            if (this.connection.State == ConnectionState.Closed)
            {
                this.connection.Open();
            }
            exists = scalarCommand.ExecuteScalar() != null;
            this.connection.Close();
            return exists;
        }

        public DataTable queryDatabase(Dictionary<string, string> columnsWithAliases, string tableToQuery, List<string> whereClauses, Dictionary<string, string> order, int numberOfResults)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            StringBuilder cmdTxt = new StringBuilder();
            cmdTxt.Append("SELECT ");
            if (columnsWithAliases.Count == 0)
            {
                cmdTxt.Append("*");
            }
            else
            {
                foreach (var column in columnsWithAliases)
                {
                    cmdTxt.AppendFormat("{0} AS \'{1}\', ", column.Key, column.Value);
                }
                cmdTxt.Length -= 2;
            }
            cmdTxt.AppendFormat(" FROM {0} ", tableToQuery);
            if (whereClauses.Count > 0)
            {
                cmdTxt.Append("WHERE ");
                for (int i = 0; i < whereClauses.Count; i++)
                {
                    cmdTxt.Append(whereClauses[i]);
                    if (i < whereClauses.Count - 1)
                    {
                        cmdTxt.Append(" AND ");
                    }
                }
            }
            if (order.Count > 0)
            {
                cmdTxt.Append(" ORDER BY ");
                foreach (var item in order)
                {
                    cmdTxt.AppendFormat("{0} {1}, ", item.Key, item.Value);
                }
                cmdTxt.Length -= 2;
            }

            if (numberOfResults > 0)
            {
                cmdTxt.AppendFormat(" LIMIT {0}", numberOfResults);
            }


            cmd.CommandText = cmdTxt.ToString();

            return this.queryDatabase(cmd);
        }

        /// <summary>
        /// Execute a server query with column aliases, where clauses, and ordering
        /// </summary>
        /// <param name="columnsWithAliases">A dictionary containing columns as keys and aliases as values. Empty dictionary will query all columns</param>
        /// <param name="tableToQuery">Table from which to retrieve data</param>
        /// <param name="whereClauses">List of where clauses as strings. Empty list will use no filtering</param>
        /// <param name="order">Ordering of results with Key as column and Value as "ASC" or "DESC". Empty dict will use no filtering</param>
        /// <returns></returns>
        public DataTable queryDatabase(Dictionary<string,string> columnsWithAliases, string tableToQuery, List<string> whereClauses, Dictionary<string, string> order)
        {
            return queryDatabase(columnsWithAliases, tableToQuery, whereClauses, order, 0);
        }

        /// <summary>
        /// Query database with WHERE clauses and no aliases
        /// </summary>
        /// <param name="col">Columns to query</param>
        /// <param name="tableToQuery">Table to query</param>
        /// <param name="whereClause">WHERE clauses</param>
        /// <returns></returns>
        public DataTable queryDatabase(List<string> columnList, string tableToQuery, List<string> whereClause)
        {
            Dictionary<string, string> cols = new Dictionary<string, string>();
            foreach (var col in columnList)
            {
                cols.Add(col, col);
            }
            return this.queryDatabase(cols, tableToQuery, whereClause, new Dictionary<string, string>());
        }

        /// <summary>
        /// Executes an insert command into the database
        /// </summary>
        /// <param name="tableName">The name of the table into which values will be inserted</param>
        /// <param name="columnsAndParams">Dictionary using the column as the key and parameter containing the value as the value</param>
        public void insertToDatabase (string tableName, Dictionary<string, SQLiteParameter> columnsAndParams)
        {
            SQLiteCommand cmd = new SQLiteCommand();
            //Create column/value strings
            StringBuilder columnsString = new StringBuilder();
            StringBuilder valuesString = new StringBuilder();

            foreach (var entry in columnsAndParams)
            {
                columnsString.Append(entry.Key);
                columnsString.Append(",");

                valuesString.Append(entry.Value.ParameterName);
                valuesString.Append(",");
                //Add parameters to SQLite command
                cmd.Parameters.Add(entry.Value);
            }

            //Remove trailing comma
            columnsString.Length--;
            valuesString.Length--;

            cmd.CommandText = string.Format("INSERT INTO {0} ({1}) VALUES ({2});", 
                tableName, columnsString.ToString(), valuesString.ToString());

            this.nonQueryDatabase(cmd);
        }

        /// <summary>
        /// Update existing values based on a unique identifier
        /// </summary>
        /// <param name="tableName">Name of table</param>
        /// <param name="idColumn">ID column</param>
        /// <param name="idValue">Unique identifier value</param>
        /// <param name="columnsAndParams"></param>
        /// 
        public void updateExistingEntry(string tableName, string idColumn, SQLiteParameter idValue, Dictionary<string, SQLiteParameter> columnsAndParams)
        {
            // update x set a=1, b=2, c=3 where x = y

            SQLiteCommand cmd = new SQLiteCommand();
            StringBuilder cmdBuilder = new StringBuilder();
            cmdBuilder.Append("UPDATE ");
            cmdBuilder.Append(tableName);
            cmdBuilder.Append(" SET");
            //Add in each colum value to the string and to the parameters list
            foreach (var entry in columnsAndParams)
            {
                cmdBuilder.AppendFormat(" {0} = {1},", entry.Key, entry.Value.ParameterName);
                cmd.Parameters.Add(entry.Value);
            }
            cmdBuilder.Length--; //Trim the traing comma
            cmdBuilder.AppendFormat(" WHERE {0} = {1}", idColumn, idValue.ParameterName);
            cmd.Parameters.Add(idValue);
            cmd.CommandText = cmdBuilder.ToString();

            this.nonQueryDatabase(cmd);

        }

        /// <summary>
        /// Gets unique values from a selected column in ASC order
        /// </summary>
        /// <param name="table"></param>
        /// <param name="col">Desired column</param>
        /// <returns></returns>
        public List<string> getUniqueEntries(string table, string col)
        {
            List<string> ls = new List<string>();
            DataTable dt = new DataTable();
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = string.Format("SELECT DISTINCT {0} FROM {1} ORDER BY {0} ASC", col, table);
            dt = this.queryDatabase(cmd);

            foreach (DataRow row in dt.Rows)
            {
                ls.Add(row[0].ToString());
            }

            return ls;
        }

        /// <summary>
        /// Gets unique values from a selected column in ASC order
        /// </summary>
        /// <param name="table"></param>
        /// <param name="col">Desired column</param>
        /// <param name="whereColumn">Column evaluated by WHERE clause</param>
        /// <param name="whereParam">Value tested for equality by WHERE clause</param>
        /// <returns></returns>
        public List<string> getUniqueEntries(string table, string col, string whereColumn, SQLiteParameter whereParam)
        {
            List<string> ls = new List<string>();
            DataTable dt = new DataTable();
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = string.Format("SELECT DISTINCT {0} FROM {1} WHERE {2} = {3} ORDER BY {0} ASC", col, table, whereColumn, whereParam);
            dt = this.queryDatabase(cmd);

            foreach (DataRow row in dt.Rows)
            {
                ls.Add(row[0].ToString());
            }

            return ls;
        }

        /// <summary>
        /// Gets unique values from a selected column in ASC order. List starts with supplied headings
        /// </summary>
        /// <param name="table">Table</param>
        /// <param name="col">Column</param>
        /// <param name="headingRows">Collection of lines to appear at the top of the list</param>
        /// <returns></returns>
        public List<string> getUniqueEntries(string table, string col, params string[] headingRows)
        {
            List<string> ls = new List<string>();
            ls.AddRange(headingRows);
            ls.AddRange(this.getUniqueEntries(table, col));
            return ls;
        }

        
       
    }

    class table
    {
        public string tableName { get; }
        public List<column> columnList { get; }

        public table(string name)
        {
            tableName = name;
        }

        /// <summary>
        /// Creates a table with columns
        /// </summary>
        /// <param name="name">Table Name</param>
        /// <param name="columns">Column</param>
        public table(string name, params column[] columns)
        {
            tableName = name;
            columnList = new List<column>();
            foreach (column col in columns)
            {
                columnList.Add(col);
            }
        }

        public void AddColumn(column columnToAdd)
        {
            columnList.Add(columnToAdd);
        }

        public string GetCreationString()
        {
            StringBuilder sb = new StringBuilder(string.Format("CREATE TABLE {0} (", tableName));
            foreach (column col in columnList)
            {
                sb.Append(string.Format("{0} {1} ",col.title, col.type));
                if (col.isPrimary)
                {
                    sb.Append("PRIMARY KEY");
                }
                else if (col.isUnique)
                {
                    sb.Append("UNIQUE");
                }
                if (col.defaultValue != string.Empty)
                {
                    sb.AppendFormat(" DEFAULT '{0}'", col.defaultValue);
                }
                sb.Append(", ");
            }
            sb.Remove((sb.Length-2), 2);
            sb.Append(")");

            return sb.ToString();

        }

        public bool Exists(SQLiteConnection conn)
        {
            string query = string.Format("SELECT * FROM sqlite_master WHERE TYPE = 'table' and NAME = '{0}'", this.tableName);
            SQLiteCommand cmd = new SQLiteCommand(query, conn);

            return (cmd.ExecuteScalar() != null);
        }
    }

    class column
    {
        public string title { get; }
        public string type { get; }
        public bool isPrimary { get; }
        public bool isUnique { get; }
        private string _defaultValue = string.Empty;
        public string defaultValue { get { return _defaultValue; } }

        public column(string Title, string Type, bool IsPrimary, bool IsUnique)
        {
            title = Title;
            type = Type;
            isPrimary = IsPrimary;
            isUnique = (isPrimary == true) ? true : IsUnique;
        }

        public column(string Title, string Type, string DefaultValue)
        {
            title = Title;
            type = Type;
            _defaultValue = DefaultValue;
        }

        public column(string Title, string Type, bool IsPrimary)
        {
            title = Title;
            type = Type;
            isPrimary = IsPrimary;
            isUnique = true;
        }

    }


}
